<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\Auth\Authorization;
use App\Security\Registration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'user_register', methods: ['GET', 'POST'])]
    public function register(Request $request, Registration $registration, MailerInterface $mailer): Response
    {   
        $authorization = new Authorization();
        return $this->render('debug.html.twig',['message' => $authorization->authorize([
            'client_id' => '4082b75f38da1c280ae858d10d1bc7f5',
            'client_secret' => 'f60b54c1ac09e803984ee5edd780d1b7',
            'username' => 'olegandreev2002@gmail.com',
            'password' => '$2y$13$gdNjucQ0Z8YTzObSvDjoMOPc8UmIosVxAsN4wOnL9zdjhrKJWwJp.',
        ])]);

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userPassword = $form->get('password')->getData();

            if ($userPassword !== $form->get('confirmedPassword')->getData()) {
                $flashType = 'error';
                $flashMessage = 'The entered passwords do not match';
            } else {
                if ($registration->store(
                    $form->get('fullName')->getData(),
                    $form->get('email')->getData(),
                    $userPassword,
                    $form->get('image')->getData(),
                )) {
                    $flashType = 'notice';
                    $flashMessage = 'To complete registration, follow the link that was sent to your email';
                    //return $this->redirectToRoute('user_login');
                    $this->addFlash($flashType, $flashMessage);
                    return $this->redirectToRoute('user_register');
                    return $this->render('auth/register.html.twig', [
                        'form' => $form->createView(),
                    ]);
                }

                $flashType = 'error';
                $flashMessage = 'Failed to register user';
            }

            $this->addFlash($flashType, $flashMessage);
        }

        return $this->render('auth/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
