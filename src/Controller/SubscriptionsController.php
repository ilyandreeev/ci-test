<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionsController extends AbstractController
{
    /**
     * @Route("/subscriptions", name="movie_subscriptions", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render("subscriptions/index.html.twig");
    }
}
