<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\File;

class AuthController extends AbstractController
{
    /**
     * @Route("/logout", name="user_logout", methods={"POST"})
     */
    public function logout(Request $request): void
    {
    }

    /**
     * @Route("/login", name="user_login", methods={"GET"})
     */
    public function login(): Response
    {
        $form = $this->createFormBuilder(null, [
                'attr'   => [
                    'id' => 'sign-in-form',
                ],
                'action' => $this->generateUrl('user_login'),
                'method' => 'POST',
            ])
            ->add('email', EmailType::class, [
                'block_name' => 'email',
            ])
            ->add('password', PasswordType::class, [
                'block_name' => 'password',
            ])
            ->add('submit', SubmitType::class, [
                'block_name' => 'submit',
            ])
            ->getForm();

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/sign-up", name="user_sign_up", methods={"GET"})
     */
    public function signUp(): Response
    {
        $form = $this->createFormBuilder(null, [
                'attr'           => [
                    'id' => 'sign-up-form',
                ],
                'action'         => $this->generateUrl('user_store'),
                'method'         => 'POST',
            ])
            ->add('image', FileType::class, [
                'block_name' => 'image',
                'required'   => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5000k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image(.png, .jpg, .jpeg)',
                    ])
                ],
            ])
            ->add('name', TextType::class, [
                'block_name' => 'name',
                'required'   => true,
                'attr'       => [
                    'maxlength' => 100,
                ]
            ])
            ->add('email', EmailType::class, [
                'block_name' => 'email',
                'required'   => true,
                'attr'       => [
                    'maxlength' => 100,
                ]
            ])
            ->add('password', PasswordType::class, [
                'block_name' => 'password',
                'required'   => true,
                'attr'       => [
                    'maxlength' => 100,
                ]
            ])
            ->add('confirm_password', PasswordType::class, [
                'block_name' => 'confirmPassword',
                'required'   => true,
                'attr'       => [
                    'maxlength' => 100,
                ]
            ])
            ->add('submit', SubmitType::class, [
                'block_name' => 'submit',
            ])
            ->getForm();

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/sign-up-store", name="user_sign_up_store", methods={"POST"})
     */
    public function store(Request $request)
    {

    }
}
