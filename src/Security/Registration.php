<?php

namespace App\Security;

use App\Entity\User;
use App\Services\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use League\Bundle\OAuth2ServerBundle\Entity\AccessToken;
use League\Bundle\OAuth2ServerBundle\Repository\ClientRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Registration
{
    private LoggerInterface $logger;
    private UserPasswordHasherInterface $userPasswordHasher;
    private EntityManagerInterface $entityManager;
    private FileUploader $fileUploader;
    private ClientRepository $clientRepository;
    private EmailVerifier $emailVerifier;
    private string $appId;

    public function __construct(
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface      $entityManager,
        EmailVerifier               $emailVerifier,
        LoggerInterface             $logger,
        FileUploader                $fileUploader,
        ClientRepository            $clientRepository,
        string                      $appId,
    ) {
        $this->userPasswordHasher = $userPasswordHasher;
        $this->entityManager = $entityManager;
        $this->emailVerifier = $emailVerifier;
        $this->logger = $logger;
        $this->fileUploader = $fileUploader;
        $this->clientRepository = $clientRepository;
        $this->appId = $appId;
    }

    /**
     * @param string            $fullName
     * @param string            $email,
     * @param string            $password
     * @param UploadedFile|null $profileImage
     * @param bool              $newsMailing
     * 
     * @return UserInterface|false
     */
    public function store(
        string        $fullName,
        string        $email,
        string        $password,
        ?UploadedFile $profileImage = null,
        bool          $newsMailing = false,
    ) {
        $user = new User();

        if (null !== $profileImage) {
            try {
                $profileImage = $this->fileUploader->upload($profileImage);
            } catch (FileException $e) {
                $this->logger->error($e->__toString());
    
                return false;
            }

            $profileImage = $profileImage->getFilename();
        }

        $user->setPassword(
                $this->userPasswordHasher->hashPassword($user, $password)
            )
            ->setFullName($fullName)
            ->setEmail($email)
            ->setNewsMailing($newsMailing)
            ->setImagePath($profileImage)
            ->setVerify(false);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->sendConfirmationMessage($user);

        return $user;
    }

    protected function sendConfirmationMessage(UserInterface $user): void
    {
        $this->emailVerifier->sendEmailConfirmation(
            'app_verify_email',
            $user,
            (new TemplatedEmail())
                ->from(new Address('ilyandreeev@gmail.com', 'My Movies'))
                ->to($user->getEmail())
                ->subject('Please confirm your email')
                ->htmlTemplate('security/confirmation_email.html.twig')
        );
    }
}
