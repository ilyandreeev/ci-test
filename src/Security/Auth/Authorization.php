<?php

namespace App\Security\Auth;

use GuzzleHttp\Client;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Session\Session;

class Authorization
{
    public function authorize(array $data, string $grantType = 'password')
    {
        $client = new Client([
            'base_uri' => 'http://my_movies_nginx:81',
        ]);

        $data['grant_type'] = $grantType;

        $response = $client->request('POST', '/token', [
            'form_params' => $data
        ]);

        $responseData = json_decode($response->getBody(), true);
        return $responseData;
    }

    /*protected function saveTokens(string $accessToken, string $refreshToken): void
    {
        $session = new Session();
        $session->start();
        $session->set('accessToken', $accessToken);
        $session->set('refreshToken', $refreshToken);

        $cache = new FilesystemAdapter();

        $accessTokenItem = $cache->getItem('accessToken');
        $accessTokenItem->set($accessToken);

        $refreshTokenItem = $cache->getItem('accessToken');
        $refreshTokenItem->set($refreshToken);

        $cache->save($accessTokenItem);
        $cache->save($refreshTokenItem);
    }*/
}
