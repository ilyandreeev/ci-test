<?php

namespace App\Entity;

use App\Repository\RolePermissionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RolePermissionRepository::class)]
class RolePermission
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Role::class, inversedBy: 'rolePermission')]
    #[ORM\JoinColumn(nullable: false)]
    private $roleId;

    #[ORM\Column(type: 'string', length: 255)]
    private $permissionKey;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoleId(): ?Role
    {
        return $this->roleId;
    }

    public function setRoleId(?Role $roleId): self
    {
        $this->roleId = $roleId;

        return $this;
    }

    public function getPermissionKey(): ?string
    {
        return $this->permissionKey;
    }

    public function setPermissionKey(string $permissionKey): self
    {
        $this->permissionKey = $permissionKey;

        return $this;
    }
}
