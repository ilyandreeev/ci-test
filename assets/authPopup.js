const authPopup = {
  data() {
    return {
      show: true
    }
  }
}

Vue.createApp(authPopup).mount('#auth-popup')