import './styles/register.css';

const app = Vue.createApp({
    methods: {
        loadImage() {
            this.$refs.inputImage.click();
        },
        showImage() {
            if (typeof(this.$refs.inputImage.files[0]) === 'undefined') {
                return;
            }

            let file = this.$refs.inputImage.files[0];

            if (!file.type.startsWith('image/')) {
                return;
            }

            let styles = [
                'upload-img',
                'rounded-circle',
            ];

            let img = document.createElement("img");

            for (let style of styles) {
                img.classList.add(style);
            }

            img.file = file;
            this.$refs.imageIcon.innerHTML = '';
            this.$refs.imageIcon.append(img);

            let reader = new FileReader();
            reader.onload = (function(aImg)
                {
                    return function(e)
                    {
                        aImg.src = e.target.result;
                    };
                })(img);
            reader.readAsDataURL(file);
        },
    },
    mounted() {
        return this.showImage();
    },
})

app.mount('#form-container')
