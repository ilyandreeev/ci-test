<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* auth_popup.html.twig */
class __TwigTemplate_192a0156058bf2d6e67331ca382747c03929fe362eefded62164f33cf40815ed extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "auth_popup.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "auth_popup.html.twig"));

        // line 1
        echo "<div id=\"auth-popup\">
    <div v-if=\"show\" class=\"position-fixed min-h-100 w-100 d-flex justify-content-center align-items-center flex-column\" style=\"background: rgba(0, 0, 0, 0.8); z-index: 100; top: 0; left: 0;\">
        <div class=\"mb-2\">
            Have an account?
            <span id=\"sign-in\" class=\"link-light text-decoration-underline\" role=\"button\">
                Sign In.
            </span>
        </div>
        <div class=\"bg-white p-5 rounded-3 position-relative overflow-auto\" style=\"width: 40vw; height: 80vh;\">   
            <button type=\"button\" class=\"ms-auto btn-close btn-close-dark position-absolute\" @click=\"show = !show\" style=\"right: 20px; top: 20px;\"></button>

            ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), 'form_start', ["attr" => ["class" => "d-flex justify-content-center align-items-center flex-column"], "method" => "POST"]);
        // line 17
        echo "

            <div id=\"sign-up-image\" role=\"button\">
                <img src=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/image_icon.svg");
        echo "\" alt=\"image\" style=\"width: 60px;\">
            </div>

            ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "image", [], "any", false, false, false, 23), 'row', ["attr" => ["class" => ""], "label_attr" => ["class" => "visually-hidden"]]);
        // line 30
        echo "

            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 32, $this->source); })()), "fullName", [], "any", false, false, false, 32), 'row', ["label" => "Name", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your name", "style" => "min-width: 400px;"]]);
        // line 41
        echo "

            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), "email", [], "any", false, false, false, 43), 'row', ["label" => "Email", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your email", "style" => "min-width: 400px;"]]);
        // line 52
        echo "

            ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 54, $this->source); })()), "password", [], "any", false, false, false, 54), 'row', ["label" => "Password", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your password", "style" => "min-width: 400px;"]]);
        // line 63
        echo "

            ";
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 65, $this->source); })()), "confirmPassword", [], "any", false, false, false, 65), 'row', ["label" => "Confirm password", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your password again", "style" => "min-width: 400px;"]]);
        // line 74
        echo "

            ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 76, $this->source); })()), "newsMailing", [], "any", false, false, false, 76), 'row', ["label" => "Receive newsletter to your mail", "label_attr" => ["class" => "text-black"]]);
        // line 81
        echo "

            <div id=\"login-services\" class=\" mb-3\">
                <span class=\"text-black me-2\">Sign up with:</span>
                <a href=\"#1\">
                    <svg enable-background=\"new 0 0 512 512\" width=\"24px\" version=\"1.1\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                        <g>
                            <path d=\"M258.8,507.2C120.4,507.8,6.6,392.6,9.9,251.9C13,117.9,124,7.2,262,8.7C398.7,10.2,511.8,124,508.1,264.9   C504.6,398.3,394.5,507.9,258.8,507.2z M40.2,258.3C41.3,383.6,142.9,480.1,262.9,478c116.2-2.1,214.7-96.8,214.7-220   c0-125.3-102.4-222.2-222.8-219.9C138.6,40.2,41,135.2,40.2,258.3z\"/>
                            <path d=\"M206.8,433.4c0-58.9,0-117.3,0-176.3c-13,0-25.6,0-38.5,0c0-20,0-39.4,0-59.4c1.7-0.1,3.4-0.3,5.2-0.3   c9.3,0,18.7-0.2,28,0.1c4.1,0.1,5.5-1,5.4-5.2c-0.2-15.2-0.2-30.3-0.1-45.5c0.1-17.1,4.9-32.6,17.1-45c11.8-12,26.9-18.5,43.3-19.5   c26.4-1.5,52.9-1.3,79.4-1.8c0.3,0,0.6,0.3,1.2,0.6c0,20.1,0,40.3,0,61c-1.9,0.1-3.7,0.2-5.4,0.2c-12,0-24,0-36,0   c-12.1,0.1-19.2,7.3-19.2,19.2c0,11.3,0,22.7,0.1,34c0,0.3,0.2,0.6,0.5,1.7c19.8,0,39.8,0,60.8,0c-2.6,20.3-5,39.7-7.6,59.8   c-18.1,0-35.8,0-54,0c0,59.2,0,117.8,0,176.6C260.1,433.4,233.9,433.4,206.8,433.4z\"/>
                        </g>
                    </svg>  
                </a>
                <a href=\"#2\" class=\"ms-2\">
                    <svg enable-background=\"new 0 0 512 512\" height=\"21px\" version=\"1.1\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                        <g>
                            <path d=\"M428.7,69.3c-24.2,24.2-48.3,48.2-71.9,71.8c-10.2-6-20.1-12.9-30.9-17.9c-41.1-19-82.6-18.9-123.6-0.1   c-38.4,17.7-64.9,47-77.8,87.2c-16.2,50.5-7.8,97.6,25.2,139.3c23.9,30.3,55.7,47.9,94,53.3c28.6,4,56.6,0.9,83.3-9.9   c37.6-15.3,61.4-42.7,70.7-82.5c0.1-0.6,0.1-1.3,0.2-2.5c-44.4,0-88.6,0-133.1,0c0-32.3,0-64,0-96.3c2.2-0.1,4.3-0.3,6.4-0.3   c73.3,0,146.7,0.1,220-0.2c5.7,0,6.9,2.5,7.7,7c4.2,22.1,5.1,44.3,2.9,66.7c-2.6,25.8-7.8,50.9-17.4,75.1   c-19,48.1-50.2,85.9-94.6,112.5C337.3,504,280.3,513,220.5,502.8c-57.6-9.9-105.9-37.6-144.5-81.5c-24.8-28.2-42-60.6-52.1-96.8   s-11.8-73-5.7-109.9C25.6,170.2,44,130.5,73.1,96c34.1-40.4,76.7-67.8,127.9-80.7c62.7-15.8,123.2-9.2,180.6,21.5   C398.6,45.9,414.3,56.8,428.7,69.3z\"/>
                        </g>
                    </svg>
                </a>
            </div>

            ";
        // line 102
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 102, $this->source); })()), "submit", [], "any", false, false, false, 102), 'row', ["label" => "Sign Up", "attr" => ["class" => "btn-dark", "style" => "min-width: 400px;"]]);
        // line 108
        echo "

            ";
        // line 110
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 110, $this->source); })()), 'rest');
        echo "

            ";
        // line 112
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 112, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        // line 116
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("auth_popup");
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "auth_popup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 116,  135 => 112,  130 => 110,  126 => 108,  124 => 102,  101 => 81,  99 => 76,  95 => 74,  93 => 65,  89 => 63,  87 => 54,  83 => 52,  81 => 43,  77 => 41,  75 => 32,  71 => 30,  69 => 23,  63 => 20,  58 => 17,  56 => 12,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"auth-popup\">
    <div v-if=\"show\" class=\"position-fixed min-h-100 w-100 d-flex justify-content-center align-items-center flex-column\" style=\"background: rgba(0, 0, 0, 0.8); z-index: 100; top: 0; left: 0;\">
        <div class=\"mb-2\">
            Have an account?
            <span id=\"sign-in\" class=\"link-light text-decoration-underline\" role=\"button\">
                Sign In.
            </span>
        </div>
        <div class=\"bg-white p-5 rounded-3 position-relative overflow-auto\" style=\"width: 40vw; height: 80vh;\">   
            <button type=\"button\" class=\"ms-auto btn-close btn-close-dark position-absolute\" @click=\"show = !show\" style=\"right: 20px; top: 20px;\"></button>

            {{ form_start(form, {
                'attr': {
                    'class': 'd-flex justify-content-center align-items-center flex-column',
                },
                'method': 'POST',
            }) }}

            <div id=\"sign-up-image\" role=\"button\">
                <img src=\"{{ asset('images/image_icon.svg') }}\" alt=\"image\" style=\"width: 60px;\">
            </div>

            {{ form_row(form.image, {
                'attr': {
                    'class': ''
                },
                'label_attr': {
                    'class': 'visually-hidden'
                },
            }) }}

            {{ form_row(form.fullName, {
                'label': 'Name',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your name',
                    'style':       'min-width: 400px;'
                }
            }) }}

            {{ form_row(form.email, {
                'label': 'Email',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your email',
                    'style':       'min-width: 400px;'
                }
            }) }}

            {{ form_row(form.password, {
                'label': 'Password',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your password',
                    'style':       'min-width: 400px;'
                }
            }) }}

            {{ form_row(form.confirmPassword, {
                'label': 'Confirm password',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your password again',
                    'style':       'min-width: 400px;'
                }
            }) }}

            {{ form_row(form.newsMailing, {
                'label': 'Receive newsletter to your mail',
                'label_attr': {
                    'class': 'text-black',
                },
            }) }}

            <div id=\"login-services\" class=\" mb-3\">
                <span class=\"text-black me-2\">Sign up with:</span>
                <a href=\"#1\">
                    <svg enable-background=\"new 0 0 512 512\" width=\"24px\" version=\"1.1\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                        <g>
                            <path d=\"M258.8,507.2C120.4,507.8,6.6,392.6,9.9,251.9C13,117.9,124,7.2,262,8.7C398.7,10.2,511.8,124,508.1,264.9   C504.6,398.3,394.5,507.9,258.8,507.2z M40.2,258.3C41.3,383.6,142.9,480.1,262.9,478c116.2-2.1,214.7-96.8,214.7-220   c0-125.3-102.4-222.2-222.8-219.9C138.6,40.2,41,135.2,40.2,258.3z\"/>
                            <path d=\"M206.8,433.4c0-58.9,0-117.3,0-176.3c-13,0-25.6,0-38.5,0c0-20,0-39.4,0-59.4c1.7-0.1,3.4-0.3,5.2-0.3   c9.3,0,18.7-0.2,28,0.1c4.1,0.1,5.5-1,5.4-5.2c-0.2-15.2-0.2-30.3-0.1-45.5c0.1-17.1,4.9-32.6,17.1-45c11.8-12,26.9-18.5,43.3-19.5   c26.4-1.5,52.9-1.3,79.4-1.8c0.3,0,0.6,0.3,1.2,0.6c0,20.1,0,40.3,0,61c-1.9,0.1-3.7,0.2-5.4,0.2c-12,0-24,0-36,0   c-12.1,0.1-19.2,7.3-19.2,19.2c0,11.3,0,22.7,0.1,34c0,0.3,0.2,0.6,0.5,1.7c19.8,0,39.8,0,60.8,0c-2.6,20.3-5,39.7-7.6,59.8   c-18.1,0-35.8,0-54,0c0,59.2,0,117.8,0,176.6C260.1,433.4,233.9,433.4,206.8,433.4z\"/>
                        </g>
                    </svg>  
                </a>
                <a href=\"#2\" class=\"ms-2\">
                    <svg enable-background=\"new 0 0 512 512\" height=\"21px\" version=\"1.1\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                        <g>
                            <path d=\"M428.7,69.3c-24.2,24.2-48.3,48.2-71.9,71.8c-10.2-6-20.1-12.9-30.9-17.9c-41.1-19-82.6-18.9-123.6-0.1   c-38.4,17.7-64.9,47-77.8,87.2c-16.2,50.5-7.8,97.6,25.2,139.3c23.9,30.3,55.7,47.9,94,53.3c28.6,4,56.6,0.9,83.3-9.9   c37.6-15.3,61.4-42.7,70.7-82.5c0.1-0.6,0.1-1.3,0.2-2.5c-44.4,0-88.6,0-133.1,0c0-32.3,0-64,0-96.3c2.2-0.1,4.3-0.3,6.4-0.3   c73.3,0,146.7,0.1,220-0.2c5.7,0,6.9,2.5,7.7,7c4.2,22.1,5.1,44.3,2.9,66.7c-2.6,25.8-7.8,50.9-17.4,75.1   c-19,48.1-50.2,85.9-94.6,112.5C337.3,504,280.3,513,220.5,502.8c-57.6-9.9-105.9-37.6-144.5-81.5c-24.8-28.2-42-60.6-52.1-96.8   s-11.8-73-5.7-109.9C25.6,170.2,44,130.5,73.1,96c34.1-40.4,76.7-67.8,127.9-80.7c62.7-15.8,123.2-9.2,180.6,21.5   C398.6,45.9,414.3,56.8,428.7,69.3z\"/>
                        </g>
                    </svg>
                </a>
            </div>

            {{ form_row(form.submit, {
                'label': 'Sign Up',
                'attr': {
                    'class': 'btn-dark',
                    'style': 'min-width: 400px;',
                }
            }) }}

            {{ form_rest(form) }}

            {{ form_end(form) }}
        </div>
    </div>
</div>
{{ encore_entry_script_tags('auth_popup') }}
", "auth_popup.html.twig", "/home/babaduk/skillup-project/templates/auth_popup.html.twig");
    }
}
