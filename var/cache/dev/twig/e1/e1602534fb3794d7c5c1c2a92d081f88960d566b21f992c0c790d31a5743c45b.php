<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* auth/register.html.twig */
class __TwigTemplate_4bd7d41f08a8b13e74543b2a45df0014e847223dcc3b4f1f7eed83d4bac0c3bd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'header' => [$this, 'block_header'],
            'form' => [$this, 'block_form'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "auth/register.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "auth/register.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "auth/register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tSign Up
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "\t";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("register");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 12
        echo "\t";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("register");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 16
        echo "\tHave an account?
\t<a href=\"#\" class=\"link-light\">
\t\tSign In.
\t</a>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 22
    public function block_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "form"));

        // line 23
        echo "\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 23, $this->source); })()), "flashes", [0 => [0 => "success", 1 => "error", 2 => "notice"]], "method", false, false, false, 23));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 24
            echo "\t\t";
            if ($context["messages"]) {
                // line 25
                echo "\t\t\t<div id=\"flash-";
                echo $context["label"];
                echo "\" class=\"p-3 rounded-3 mb-3\">
\t\t\t\t<div id=\"flash-label\" class=\"mb-3 text-center\">
\t\t\t\t\t";
                // line 27
                echo twig_upper_filter($this->env, $context["label"]);
                echo "!
\t\t\t\t</div>
\t\t\t\t";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 30
                    echo "\t\t\t\t\t<li class=\"flash-message ms-2 text-center\">
\t\t\t\t\t\t";
                    // line 31
                    echo $context["message"];
                    echo "
\t\t\t\t\t</li>
\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "\t\t\t</div>
\t\t";
            }
            // line 36
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "
\t";
        // line 38
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 38, $this->source); })()), 'form_start', ["attr" => ["class" => "d-flex justify-content-center align-items-center flex-column"], "method" => "POST"]);
        // line 43
        echo "

\t<div @click=\"loadImage\" id=\"profile-image\" role=\"button\" title=\"Upload image\" class=\"bg-white my-2 position-relative\" ref=\"uploadImage\">
\t\t<div ref=\"imageIcon\">
\t\t\t<svg style=\"enable-background:new 0 0 512 512;\" fill=\"black\" version=\"1.1\" viewbox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
\t\t\t\t<g>
\t\t\t\t\t<g>
\t\t\t\t\t\t<circle cx=\"256\" cy=\"256\" r=\"250\"/>
\t\t\t\t\t</g>
\t\t\t\t\t<path class=\"st1\" d=\"M506,259.6c-0.2-0.3-0.5-0.6-0.8-1c-33.1-33.1-66.2-66.1-99.3-99.3c-2.7-2.7-5.6-3.9-9.3-3.9   c-46.6,0.1-93.1,0-139.7,0c-46.6,0-93.3,0-139.9,0c-1,0-2.1,0-3.1,0.2c-3.5,0.5-6.2,3-6.9,6.4c-0.2,1.2-0.2,2.4-0.2,3.6   c0,60.6,0,121.3-0.1,181.9c0,3.6,1.1,6.4,3.7,8.9c21.3,21.2,42.6,42.5,63.8,63.8c28.2,28.2,56.5,56.4,84.6,84.7   c0.4,0.4,0.8,0.8,1.3,1C395.1,503.8,504.1,394.6,506,259.6z\"/>
\t\t\t\t\t<g>
\t\t\t\t\t\t<path fill=\"white\" d=\"M114.4,357h283.2c4.6,0,8.4-3.8,8.4-8.4V163.4c0-4.6-3.8-8.4-8.4-8.4H114.4c-4.6,0-8.4,3.8-8.4,8.4v185.2    C106,353.2,109.8,357,114.4,357z M122.8,326.8l89.8-89.8l103.3,103.3H122.8V326.8z M339.6,340.2l-35.4-35.4l26.3-26.3l56.8,56.8    c0.6,0.6,1.2,1.1,1.9,1.4v3.4L339.6,340.2L339.6,340.2z M389.2,171.8v141.9l-52.8-52.8c-3.3-3.3-8.6-3.3-11.9,0L292.4,293    l-73.8-73.8c-3.3-3.3-8.6-3.3-11.9,0l-83.9,83.9V171.8H389.2z\"/>
\t\t\t\t\t\t<path fill=\"white\" d=\"M291.9,251c17.5,0,31.7-14.2,31.7-31.7s-14.2-31.7-31.7-31.7s-31.7,14.2-31.7,31.7S274.4,251,291.9,251z     M291.9,204.4c8.2,0,14.9,6.7,14.9,14.9s-6.7,14.9-14.9,14.9s-14.9-6.7-14.9-14.9S283.6,204.4,291.9,204.4z\"/>
\t\t\t\t\t</g>
\t\t\t\t</g>
\t\t\t</svg>
\t\t</div>
\t\t<svg id=\"add-icon\" class=\"position-absolute\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewbox=\"0 0 129 129\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" enable-background=\"new 0 0 129 129\">
\t\t\t<g>
\t\t\t\t<g>
\t\t\t\t\t<path d=\"M64.5,6.5c-32,0-58.1,26-58.1,58s26,58,58.1,58c32,0,58-26,58-58S96.5,6.5,64.5,6.5z M64.5,114.4    C37,114.4,14.6,92,14.6,64.5S37,14.6,64.5,14.6c27.5,0,49.9,22.4,49.9,49.9S92,114.4,64.5,114.4z\"/>
\t\t\t\t\t<path d=\"m91.2,59.9h-22.6v-22.7c0-2.3-1.8-4.1-4.1-4.1-2.3,0-4.1,1.8-4.1,4.1v22.6h-22.6c-2.3,0-4.1,1.8-4.1,4.1s1.8,4.1 4.1,4.1h22.6v22.6c0,2.3 1.8,4.1 4.1,4.1 2.3,0 4.1-1.8 4.1-4.1v-22.6h22.6c2.3,0 4.1-1.8 4.1-4.1s-1.8-4-4.1-4z\"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t</svg>
\t</div>
\t";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 69, $this->source); })()), "image", [], "any", false, false, false, 69), 'row', ["attr" => ["ref" => "inputImage", "class" => "visually-hidden", "@change" => "showImage"], "label_attr" => ["class" => "visually-hidden"]]);
        // line 78
        echo "

\t";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 80, $this->source); })()), "fullName", [], "any", false, false, false, 80), 'row', ["label" => "Name", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your name", "style" => "min-width: 400px;"]]);
        // line 89
        echo "

\t";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 91, $this->source); })()), "email", [], "any", false, false, false, 91), 'row', ["label" => "Email", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your email", "style" => "min-width: 400px;", "value" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 99
($context["form"] ?? null), "children", [], "any", false, true, false, 99), "email", [], "any", false, true, false, 99), "vars", [], "any", false, true, false, 99), "value", [], "any", true, true, false, 99) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 99), "email", [], "any", false, true, false, 99), "vars", [], "any", false, true, false, 99), "value", [], "any", false, false, false, 99)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 99), "email", [], "any", false, true, false, 99), "vars", [], "any", false, true, false, 99), "value", [], "any", false, false, false, 99)) : (""))]]);
        // line 101
        echo "

\t";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 103, $this->source); })()), "password", [], "any", false, false, false, 103), 'row', ["label" => "Password", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your password", "style" => "min-width: 400px;"]]);
        // line 112
        echo "

\t";
        // line 114
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 114, $this->source); })()), "confirmedPassword", [], "any", false, false, false, 114), 'row', ["label" => "Confirm password", "label_attr" => ["class" => "fs-4 text-black"], "attr" => ["placeholder" => "Enter your password again", "style" => "min-width: 400px;"]]);
        // line 123
        echo "

\t";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 125, $this->source); })()), "newsMailing", [], "any", false, false, false, 125), 'row', ["label" => "Receive newsletter to your email", "label_attr" => ["class" => "text-black"], "attr" => ["checked" => (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 131
($context["form"] ?? null), "children", [], "any", false, true, false, 131), "newsMailing", [], "any", false, true, false, 131), "vars", [], "any", false, true, false, 131), "value", [], "any", true, true, false, 131) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 131), "newsMailing", [], "any", false, true, false, 131), "vars", [], "any", false, true, false, 131), "value", [], "any", false, false, false, 131)))) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 131), "newsMailing", [], "any", false, true, false, 131), "vars", [], "any", false, true, false, 131), "value", [], "any", false, false, false, 131)) : (0))]]);
        // line 133
        echo "

\t<div id=\"register-services\" class=\" mb-3\">
\t\t<span class=\"text-black me-2\">Sign up with:</span>
\t\t<a href=\"#1\">
\t\t\t<svg enable-background=\"new 0 0 512 512\" width=\"24px\" version=\"1.1\" viewbox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
\t\t\t\t<g>
\t\t\t\t\t<path d=\"M258.8,507.2C120.4,507.8,6.6,392.6,9.9,251.9C13,117.9,124,7.2,262,8.7C398.7,10.2,511.8,124,508.1,264.9   C504.6,398.3,394.5,507.9,258.8,507.2z M40.2,258.3C41.3,383.6,142.9,480.1,262.9,478c116.2-2.1,214.7-96.8,214.7-220   c0-125.3-102.4-222.2-222.8-219.9C138.6,40.2,41,135.2,40.2,258.3z\"/>
\t\t\t\t\t<path d=\"M206.8,433.4c0-58.9,0-117.3,0-176.3c-13,0-25.6,0-38.5,0c0-20,0-39.4,0-59.4c1.7-0.1,3.4-0.3,5.2-0.3   c9.3,0,18.7-0.2,28,0.1c4.1,0.1,5.5-1,5.4-5.2c-0.2-15.2-0.2-30.3-0.1-45.5c0.1-17.1,4.9-32.6,17.1-45c11.8-12,26.9-18.5,43.3-19.5   c26.4-1.5,52.9-1.3,79.4-1.8c0.3,0,0.6,0.3,1.2,0.6c0,20.1,0,40.3,0,61c-1.9,0.1-3.7,0.2-5.4,0.2c-12,0-24,0-36,0   c-12.1,0.1-19.2,7.3-19.2,19.2c0,11.3,0,22.7,0.1,34c0,0.3,0.2,0.6,0.5,1.7c19.8,0,39.8,0,60.8,0c-2.6,20.3-5,39.7-7.6,59.8   c-18.1,0-35.8,0-54,0c0,59.2,0,117.8,0,176.6C260.1,433.4,233.9,433.4,206.8,433.4z\"/>
\t\t\t\t</g>
\t\t\t</svg>
\t\t</a>
\t\t<a href=\"#2\" class=\"ms-2\">
\t\t\t<svg enable-background=\"new 0 0 512 512\" height=\"21px\" version=\"1.1\" viewbox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
\t\t\t\t<g>
\t\t\t\t\t<path d=\"M428.7,69.3c-24.2,24.2-48.3,48.2-71.9,71.8c-10.2-6-20.1-12.9-30.9-17.9c-41.1-19-82.6-18.9-123.6-0.1   c-38.4,17.7-64.9,47-77.8,87.2c-16.2,50.5-7.8,97.6,25.2,139.3c23.9,30.3,55.7,47.9,94,53.3c28.6,4,56.6,0.9,83.3-9.9   c37.6-15.3,61.4-42.7,70.7-82.5c0.1-0.6,0.1-1.3,0.2-2.5c-44.4,0-88.6,0-133.1,0c0-32.3,0-64,0-96.3c2.2-0.1,4.3-0.3,6.4-0.3   c73.3,0,146.7,0.1,220-0.2c5.7,0,6.9,2.5,7.7,7c4.2,22.1,5.1,44.3,2.9,66.7c-2.6,25.8-7.8,50.9-17.4,75.1   c-19,48.1-50.2,85.9-94.6,112.5C337.3,504,280.3,513,220.5,502.8c-57.6-9.9-105.9-37.6-144.5-81.5c-24.8-28.2-42-60.6-52.1-96.8   s-11.8-73-5.7-109.9C25.6,170.2,44,130.5,73.1,96c34.1-40.4,76.7-67.8,127.9-80.7c62.7-15.8,123.2-9.2,180.6,21.5   C398.6,45.9,414.3,56.8,428.7,69.3z\"/>
\t\t\t\t</g>
\t\t\t</svg>
\t\t</a>
\t</div>

\t";
        // line 154
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 154, $this->source); })()), "submit", [], "any", false, false, false, 154), 'row', ["label" => "Sign Up", "attr" => ["class" => "btn-dark", "style" => "min-width: 400px;"]]);
        // line 160
        echo "

\t";
        // line 162
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 162, $this->source); })()), 'rest');
        echo "

\t";
        // line 164
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 164, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "auth/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 164,  304 => 162,  300 => 160,  298 => 154,  275 => 133,  273 => 131,  272 => 125,  268 => 123,  266 => 114,  262 => 112,  260 => 103,  256 => 101,  254 => 99,  253 => 91,  249 => 89,  247 => 80,  243 => 78,  241 => 69,  213 => 43,  211 => 38,  208 => 37,  202 => 36,  198 => 34,  189 => 31,  186 => 30,  182 => 29,  177 => 27,  171 => 25,  168 => 24,  163 => 23,  153 => 22,  139 => 16,  129 => 15,  116 => 12,  106 => 11,  93 => 8,  83 => 7,  72 => 4,  62 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}
\tSign Up
{% endblock %}

{% block stylesheets %}
\t{{ encore_entry_link_tags('register') }}
{% endblock %}

{% block javascripts %}
\t{{ encore_entry_script_tags('register') }}
{% endblock %}

{% block header %}
\tHave an account?
\t<a href=\"#\" class=\"link-light\">
\t\tSign In.
\t</a>
{% endblock %}

{% block form %}
\t{% for label, messages in app.flashes(['success', 'error', 'notice']) %}
\t\t{% if messages %}
\t\t\t<div id=\"flash-{{ label }}\" class=\"p-3 rounded-3 mb-3\">
\t\t\t\t<div id=\"flash-label\" class=\"mb-3 text-center\">
\t\t\t\t\t{{ label|upper }}!
\t\t\t\t</div>
\t\t\t\t{% for message in messages %}
\t\t\t\t\t<li class=\"flash-message ms-2 text-center\">
\t\t\t\t\t\t{{ message }}
\t\t\t\t\t</li>
\t\t\t\t{% endfor %}
\t\t\t</div>
\t\t{% endif %}
\t{% endfor %}

\t{{ form_start(form, {
                'attr': {
                    'class': 'd-flex justify-content-center align-items-center flex-column',
                },
                'method': 'POST',
            }) }}

\t<div @click=\"loadImage\" id=\"profile-image\" role=\"button\" title=\"Upload image\" class=\"bg-white my-2 position-relative\" ref=\"uploadImage\">
\t\t<div ref=\"imageIcon\">
\t\t\t<svg style=\"enable-background:new 0 0 512 512;\" fill=\"black\" version=\"1.1\" viewbox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
\t\t\t\t<g>
\t\t\t\t\t<g>
\t\t\t\t\t\t<circle cx=\"256\" cy=\"256\" r=\"250\"/>
\t\t\t\t\t</g>
\t\t\t\t\t<path class=\"st1\" d=\"M506,259.6c-0.2-0.3-0.5-0.6-0.8-1c-33.1-33.1-66.2-66.1-99.3-99.3c-2.7-2.7-5.6-3.9-9.3-3.9   c-46.6,0.1-93.1,0-139.7,0c-46.6,0-93.3,0-139.9,0c-1,0-2.1,0-3.1,0.2c-3.5,0.5-6.2,3-6.9,6.4c-0.2,1.2-0.2,2.4-0.2,3.6   c0,60.6,0,121.3-0.1,181.9c0,3.6,1.1,6.4,3.7,8.9c21.3,21.2,42.6,42.5,63.8,63.8c28.2,28.2,56.5,56.4,84.6,84.7   c0.4,0.4,0.8,0.8,1.3,1C395.1,503.8,504.1,394.6,506,259.6z\"/>
\t\t\t\t\t<g>
\t\t\t\t\t\t<path fill=\"white\" d=\"M114.4,357h283.2c4.6,0,8.4-3.8,8.4-8.4V163.4c0-4.6-3.8-8.4-8.4-8.4H114.4c-4.6,0-8.4,3.8-8.4,8.4v185.2    C106,353.2,109.8,357,114.4,357z M122.8,326.8l89.8-89.8l103.3,103.3H122.8V326.8z M339.6,340.2l-35.4-35.4l26.3-26.3l56.8,56.8    c0.6,0.6,1.2,1.1,1.9,1.4v3.4L339.6,340.2L339.6,340.2z M389.2,171.8v141.9l-52.8-52.8c-3.3-3.3-8.6-3.3-11.9,0L292.4,293    l-73.8-73.8c-3.3-3.3-8.6-3.3-11.9,0l-83.9,83.9V171.8H389.2z\"/>
\t\t\t\t\t\t<path fill=\"white\" d=\"M291.9,251c17.5,0,31.7-14.2,31.7-31.7s-14.2-31.7-31.7-31.7s-31.7,14.2-31.7,31.7S274.4,251,291.9,251z     M291.9,204.4c8.2,0,14.9,6.7,14.9,14.9s-6.7,14.9-14.9,14.9s-14.9-6.7-14.9-14.9S283.6,204.4,291.9,204.4z\"/>
\t\t\t\t\t</g>
\t\t\t\t</g>
\t\t\t</svg>
\t\t</div>
\t\t<svg id=\"add-icon\" class=\"position-absolute\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewbox=\"0 0 129 129\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" enable-background=\"new 0 0 129 129\">
\t\t\t<g>
\t\t\t\t<g>
\t\t\t\t\t<path d=\"M64.5,6.5c-32,0-58.1,26-58.1,58s26,58,58.1,58c32,0,58-26,58-58S96.5,6.5,64.5,6.5z M64.5,114.4    C37,114.4,14.6,92,14.6,64.5S37,14.6,64.5,14.6c27.5,0,49.9,22.4,49.9,49.9S92,114.4,64.5,114.4z\"/>
\t\t\t\t\t<path d=\"m91.2,59.9h-22.6v-22.7c0-2.3-1.8-4.1-4.1-4.1-2.3,0-4.1,1.8-4.1,4.1v22.6h-22.6c-2.3,0-4.1,1.8-4.1,4.1s1.8,4.1 4.1,4.1h22.6v22.6c0,2.3 1.8,4.1 4.1,4.1 2.3,0 4.1-1.8 4.1-4.1v-22.6h22.6c2.3,0 4.1-1.8 4.1-4.1s-1.8-4-4.1-4z\"/>
\t\t\t\t</g>
\t\t\t</g>
\t\t</svg>
\t</div>
\t{{ form_row(form.image, {
        'attr': {
            'ref': \t   'inputImage',
            'class':   'visually-hidden',
            '@change': 'showImage',
        },
        'label_attr': {
            'class': 'visually-hidden',
        },
    }) }}

\t{{ form_row(form.fullName, {
                'label': 'Name',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your name',
                    'style':       'min-width: 400px;'
                }
            }) }}

\t{{ form_row(form.email, {
                'label': 'Email',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your email',
                    'style':       'min-width: 400px;',
                    'value':        form.children.email.vars.value ?? '',
                }
            }) }}

\t{{ form_row(form.password, {
                'label': 'Password',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your password',
                    'style':       'min-width: 400px;'
                }
            }) }}

\t{{ form_row(form.confirmedPassword, {
                'label': 'Confirm password',
                'label_attr': {
                    'class': 'fs-4 text-black',
                },
                'attr': {
                    'placeholder': 'Enter your password again',
                    'style':       'min-width: 400px;',
                }
            }) }}

\t{{ form_row(form.newsMailing, {
                'label': 'Receive newsletter to your email',
                'label_attr': {
                    'class': 'text-black',
                },
                'attr': {
                    'checked': form.children.newsMailing.vars.value ?? 0,
                },
            }) }}

\t<div id=\"register-services\" class=\" mb-3\">
\t\t<span class=\"text-black me-2\">Sign up with:</span>
\t\t<a href=\"#1\">
\t\t\t<svg enable-background=\"new 0 0 512 512\" width=\"24px\" version=\"1.1\" viewbox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
\t\t\t\t<g>
\t\t\t\t\t<path d=\"M258.8,507.2C120.4,507.8,6.6,392.6,9.9,251.9C13,117.9,124,7.2,262,8.7C398.7,10.2,511.8,124,508.1,264.9   C504.6,398.3,394.5,507.9,258.8,507.2z M40.2,258.3C41.3,383.6,142.9,480.1,262.9,478c116.2-2.1,214.7-96.8,214.7-220   c0-125.3-102.4-222.2-222.8-219.9C138.6,40.2,41,135.2,40.2,258.3z\"/>
\t\t\t\t\t<path d=\"M206.8,433.4c0-58.9,0-117.3,0-176.3c-13,0-25.6,0-38.5,0c0-20,0-39.4,0-59.4c1.7-0.1,3.4-0.3,5.2-0.3   c9.3,0,18.7-0.2,28,0.1c4.1,0.1,5.5-1,5.4-5.2c-0.2-15.2-0.2-30.3-0.1-45.5c0.1-17.1,4.9-32.6,17.1-45c11.8-12,26.9-18.5,43.3-19.5   c26.4-1.5,52.9-1.3,79.4-1.8c0.3,0,0.6,0.3,1.2,0.6c0,20.1,0,40.3,0,61c-1.9,0.1-3.7,0.2-5.4,0.2c-12,0-24,0-36,0   c-12.1,0.1-19.2,7.3-19.2,19.2c0,11.3,0,22.7,0.1,34c0,0.3,0.2,0.6,0.5,1.7c19.8,0,39.8,0,60.8,0c-2.6,20.3-5,39.7-7.6,59.8   c-18.1,0-35.8,0-54,0c0,59.2,0,117.8,0,176.6C260.1,433.4,233.9,433.4,206.8,433.4z\"/>
\t\t\t\t</g>
\t\t\t</svg>
\t\t</a>
\t\t<a href=\"#2\" class=\"ms-2\">
\t\t\t<svg enable-background=\"new 0 0 512 512\" height=\"21px\" version=\"1.1\" viewbox=\"0 0 512 512\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
\t\t\t\t<g>
\t\t\t\t\t<path d=\"M428.7,69.3c-24.2,24.2-48.3,48.2-71.9,71.8c-10.2-6-20.1-12.9-30.9-17.9c-41.1-19-82.6-18.9-123.6-0.1   c-38.4,17.7-64.9,47-77.8,87.2c-16.2,50.5-7.8,97.6,25.2,139.3c23.9,30.3,55.7,47.9,94,53.3c28.6,4,56.6,0.9,83.3-9.9   c37.6-15.3,61.4-42.7,70.7-82.5c0.1-0.6,0.1-1.3,0.2-2.5c-44.4,0-88.6,0-133.1,0c0-32.3,0-64,0-96.3c2.2-0.1,4.3-0.3,6.4-0.3   c73.3,0,146.7,0.1,220-0.2c5.7,0,6.9,2.5,7.7,7c4.2,22.1,5.1,44.3,2.9,66.7c-2.6,25.8-7.8,50.9-17.4,75.1   c-19,48.1-50.2,85.9-94.6,112.5C337.3,504,280.3,513,220.5,502.8c-57.6-9.9-105.9-37.6-144.5-81.5c-24.8-28.2-42-60.6-52.1-96.8   s-11.8-73-5.7-109.9C25.6,170.2,44,130.5,73.1,96c34.1-40.4,76.7-67.8,127.9-80.7c62.7-15.8,123.2-9.2,180.6,21.5   C398.6,45.9,414.3,56.8,428.7,69.3z\"/>
\t\t\t\t</g>
\t\t\t</svg>
\t\t</a>
\t</div>

\t{{ form_row(form.submit, {
                'label': 'Sign Up',
                'attr': {
                    'class': 'btn-dark',
                    'style': 'min-width: 400px;',
                }
            }) }}

\t{{ form_rest(form) }}

\t{{ form_end(form) }}
{% endblock %}
", "auth/register.html.twig", "/home/babaduk/skillup-project/templates/auth/register.html.twig");
    }
}
