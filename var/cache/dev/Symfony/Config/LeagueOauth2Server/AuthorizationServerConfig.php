<?php

namespace Symfony\Config\LeagueOauth2Server;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 */
class AuthorizationServerConfig 
{
    private $privateKey;
    private $privateKeyPassphrase;
    private $encryptionKey;
    private $encryptionKeyType;
    private $accessTokenTtl;
    private $refreshTokenTtl;
    private $authCodeTtl;
    private $enableClientCredentialsGrant;
    private $enablePasswordGrant;
    private $enableRefreshTokenGrant;
    private $enableAuthCodeGrant;
    private $requireCodeChallengeForPublicClients;
    private $enableImplicitGrant;
    private $persistAccessToken;
    
    /**
     * Full path to the private key file.
    How to generate a private key: https://oauth2.thephpleague.com/installation/#generating-public-and-private-keys
     * @example /var/oauth/private.key
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function privateKey($value): static
    {
        $this->privateKey = $value;
    
        return $this;
    }
    
    /**
     * Passphrase of the private key, if any
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function privateKeyPassphrase($value): static
    {
        $this->privateKeyPassphrase = $value;
    
        return $this;
    }
    
    /**
     * The plain string or the ascii safe string used to create a Defuse\Crypto\Key to be used as an encryption key.
    How to generate an encryption key: https://oauth2.thephpleague.com/installation/#string-password
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function encryptionKey($value): static
    {
        $this->encryptionKey = $value;
    
        return $this;
    }
    
    /**
     * The type of value of 'encryption_key'
    Should be either 'plain' or 'defuse'
     * @default 'plain'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function encryptionKeyType($value): static
    {
        $this->encryptionKeyType = $value;
    
        return $this;
    }
    
    /**
     * How long the issued access token should be valid for.
    The value should be a valid interval: http://php.net/manual/en/dateinterval.construct.php#refsect1-dateinterval.construct-parameters
     * @default 'PT1H'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function accessTokenTtl($value): static
    {
        $this->accessTokenTtl = $value;
    
        return $this;
    }
    
    /**
     * How long the issued refresh token should be valid for.
    The value should be a valid interval: http://php.net/manual/en/dateinterval.construct.php#refsect1-dateinterval.construct-parameters
     * @default 'P1M'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function refreshTokenTtl($value): static
    {
        $this->refreshTokenTtl = $value;
    
        return $this;
    }
    
    /**
     * How long the issued auth code should be valid for.
    The value should be a valid interval: http://php.net/manual/en/dateinterval.construct.php#refsect1-dateinterval.construct-parameters
     * @default 'PT10M'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function authCodeTtl($value): static
    {
        $this->authCodeTtl = $value;
    
        return $this;
    }
    
    /**
     * Whether to enable the client credentials grant
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enableClientCredentialsGrant($value): static
    {
        $this->enableClientCredentialsGrant = $value;
    
        return $this;
    }
    
    /**
     * Whether to enable the password grant
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enablePasswordGrant($value): static
    {
        $this->enablePasswordGrant = $value;
    
        return $this;
    }
    
    /**
     * Whether to enable the refresh token grant
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enableRefreshTokenGrant($value): static
    {
        $this->enableRefreshTokenGrant = $value;
    
        return $this;
    }
    
    /**
     * Whether to enable the authorization code grant
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enableAuthCodeGrant($value): static
    {
        $this->enableAuthCodeGrant = $value;
    
        return $this;
    }
    
    /**
     * Whether to require code challenge for public clients for the auth code grant
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function requireCodeChallengeForPublicClients($value): static
    {
        $this->requireCodeChallengeForPublicClients = $value;
    
        return $this;
    }
    
    /**
     * Whether to enable the implicit grant
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function enableImplicitGrant($value): static
    {
        $this->enableImplicitGrant = $value;
    
        return $this;
    }
    
    /**
     * Whether to enable access token saving to persistence layer
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function persistAccessToken($value): static
    {
        $this->persistAccessToken = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['private_key'])) {
            $this->privateKey = $value['private_key'];
            unset($value['private_key']);
        }
    
        if (isset($value['private_key_passphrase'])) {
            $this->privateKeyPassphrase = $value['private_key_passphrase'];
            unset($value['private_key_passphrase']);
        }
    
        if (isset($value['encryption_key'])) {
            $this->encryptionKey = $value['encryption_key'];
            unset($value['encryption_key']);
        }
    
        if (isset($value['encryption_key_type'])) {
            $this->encryptionKeyType = $value['encryption_key_type'];
            unset($value['encryption_key_type']);
        }
    
        if (isset($value['access_token_ttl'])) {
            $this->accessTokenTtl = $value['access_token_ttl'];
            unset($value['access_token_ttl']);
        }
    
        if (isset($value['refresh_token_ttl'])) {
            $this->refreshTokenTtl = $value['refresh_token_ttl'];
            unset($value['refresh_token_ttl']);
        }
    
        if (isset($value['auth_code_ttl'])) {
            $this->authCodeTtl = $value['auth_code_ttl'];
            unset($value['auth_code_ttl']);
        }
    
        if (isset($value['enable_client_credentials_grant'])) {
            $this->enableClientCredentialsGrant = $value['enable_client_credentials_grant'];
            unset($value['enable_client_credentials_grant']);
        }
    
        if (isset($value['enable_password_grant'])) {
            $this->enablePasswordGrant = $value['enable_password_grant'];
            unset($value['enable_password_grant']);
        }
    
        if (isset($value['enable_refresh_token_grant'])) {
            $this->enableRefreshTokenGrant = $value['enable_refresh_token_grant'];
            unset($value['enable_refresh_token_grant']);
        }
    
        if (isset($value['enable_auth_code_grant'])) {
            $this->enableAuthCodeGrant = $value['enable_auth_code_grant'];
            unset($value['enable_auth_code_grant']);
        }
    
        if (isset($value['require_code_challenge_for_public_clients'])) {
            $this->requireCodeChallengeForPublicClients = $value['require_code_challenge_for_public_clients'];
            unset($value['require_code_challenge_for_public_clients']);
        }
    
        if (isset($value['enable_implicit_grant'])) {
            $this->enableImplicitGrant = $value['enable_implicit_grant'];
            unset($value['enable_implicit_grant']);
        }
    
        if (isset($value['persist_access_token'])) {
            $this->persistAccessToken = $value['persist_access_token'];
            unset($value['persist_access_token']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->privateKey) {
            $output['private_key'] = $this->privateKey;
        }
        if (null !== $this->privateKeyPassphrase) {
            $output['private_key_passphrase'] = $this->privateKeyPassphrase;
        }
        if (null !== $this->encryptionKey) {
            $output['encryption_key'] = $this->encryptionKey;
        }
        if (null !== $this->encryptionKeyType) {
            $output['encryption_key_type'] = $this->encryptionKeyType;
        }
        if (null !== $this->accessTokenTtl) {
            $output['access_token_ttl'] = $this->accessTokenTtl;
        }
        if (null !== $this->refreshTokenTtl) {
            $output['refresh_token_ttl'] = $this->refreshTokenTtl;
        }
        if (null !== $this->authCodeTtl) {
            $output['auth_code_ttl'] = $this->authCodeTtl;
        }
        if (null !== $this->enableClientCredentialsGrant) {
            $output['enable_client_credentials_grant'] = $this->enableClientCredentialsGrant;
        }
        if (null !== $this->enablePasswordGrant) {
            $output['enable_password_grant'] = $this->enablePasswordGrant;
        }
        if (null !== $this->enableRefreshTokenGrant) {
            $output['enable_refresh_token_grant'] = $this->enableRefreshTokenGrant;
        }
        if (null !== $this->enableAuthCodeGrant) {
            $output['enable_auth_code_grant'] = $this->enableAuthCodeGrant;
        }
        if (null !== $this->requireCodeChallengeForPublicClients) {
            $output['require_code_challenge_for_public_clients'] = $this->requireCodeChallengeForPublicClients;
        }
        if (null !== $this->enableImplicitGrant) {
            $output['enable_implicit_grant'] = $this->enableImplicitGrant;
        }
        if (null !== $this->persistAccessToken) {
            $output['persist_access_token'] = $this->persistAccessToken;
        }
    
        return $output;
    }

}
