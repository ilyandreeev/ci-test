<?php

namespace Symfony\Config\LeagueOauth2Server;

require_once __DIR__.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'DoctrineConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\Loader\ParamConfigurator;


/**
 * This class is automatically generated to help creating config.
 */
class PersistenceConfig 
{
    private $doctrine;
    private $inMemory;
    
    public function doctrine(array $value = []): \Symfony\Config\LeagueOauth2Server\Persistence\DoctrineConfig
    {
        if (null === $this->doctrine) {
            $this->doctrine = new \Symfony\Config\LeagueOauth2Server\Persistence\DoctrineConfig($value);
        } elseif ([] !== $value) {
            throw new InvalidConfigurationException('The node created by "doctrine()" has already been initialized. You cannot pass values the second time you call doctrine().');
        }
    
        return $this->doctrine;
    }
    
    /**
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function inMemory($value): static
    {
        $this->inMemory = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['doctrine'])) {
            $this->doctrine = new \Symfony\Config\LeagueOauth2Server\Persistence\DoctrineConfig($value['doctrine']);
            unset($value['doctrine']);
        }
    
        if (isset($value['in_memory'])) {
            $this->inMemory = $value['in_memory'];
            unset($value['in_memory']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->doctrine) {
            $output['doctrine'] = $this->doctrine->toArray();
        }
        if (null !== $this->inMemory) {
            $output['in_memory'] = $this->inMemory;
        }
    
        return $output;
    }

}
