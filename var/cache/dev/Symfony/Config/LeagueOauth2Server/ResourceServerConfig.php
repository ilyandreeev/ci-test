<?php

namespace Symfony\Config\LeagueOauth2Server;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 */
class ResourceServerConfig 
{
    private $publicKey;
    
    /**
     * Full path to the public key file
    How to generate a public key: https://oauth2.thephpleague.com/installation/#generating-public-and-private-keys
     * @example /var/oauth/public.key
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function publicKey($value): static
    {
        $this->publicKey = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['public_key'])) {
            $this->publicKey = $value['public_key'];
            unset($value['public_key']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->publicKey) {
            $output['public_key'] = $this->publicKey;
        }
    
        return $output;
    }

}
