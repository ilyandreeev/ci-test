<?php

namespace Symfony\Config\LeagueOauth2Server;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 */
class ScopesConfig 
{
    private $available;
    private $default;
    
    /**
     * @param ParamConfigurator|list<ParamConfigurator|mixed> $value
     *
     * @return $this
     */
    public function available(ParamConfigurator|array $value): static
    {
        $this->available = $value;
    
        return $this;
    }
    
    /**
     * @param ParamConfigurator|list<ParamConfigurator|mixed> $value
     *
     * @return $this
     */
    public function default(ParamConfigurator|array $value): static
    {
        $this->default = $value;
    
        return $this;
    }
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['available'])) {
            $this->available = $value['available'];
            unset($value['available']);
        }
    
        if (isset($value['default'])) {
            $this->default = $value['default'];
            unset($value['default']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->available) {
            $output['available'] = $this->available;
        }
        if (null !== $this->default) {
            $output['default'] = $this->default;
        }
    
        return $output;
    }

}
