<?php

namespace ContainerTVaw2x2;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getRegistrationService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Security\Registration' shared autowired service.
     *
     * @return \App\Security\Registration
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Security/Registration.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/password-hasher/Hasher/UserPasswordHasherInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/password-hasher/Hasher/UserPasswordHasher.php';
        include_once \dirname(__DIR__, 4).'/src/Services/FileUploader.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/string/Slugger/SluggerInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/string/Slugger/AsciiSlugger.php';

        return $container->privates['App\\Security\\Registration'] = new \App\Security\Registration(new \Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher(($container->privates['security.password_hasher_factory'] ?? $container->load('getSecurity_PasswordHasherFactoryService'))), ($container->services['doctrine.orm.default_entity_manager'] ?? $container->getDoctrine_Orm_DefaultEntityManagerService()), ($container->privates['App\\Security\\EmailVerifier'] ?? $container->load('getEmailVerifierService')), ($container->privates['monolog.logger'] ?? $container->getMonolog_LoggerService()), new \App\Services\FileUploader('uploads/images', ($container->privates['slugger'] ?? ($container->privates['slugger'] = new \Symfony\Component\String\Slugger\AsciiSlugger('en')))), ($container->privates['league.oauth2_server.repository.client'] ?? $container->load('getLeague_Oauth2Server_Repository_ClientService')), $container->getEnv('APP_ID'));
    }
}
